# tjeneste

Very simple web-framework.

## Installation

Add it to `Projectfile`

```crystal
deps do
  github "ragmaanir/tjeneste"
end
```

## Usage

```crystal
require "tjeneste"
```

TODO: Write usage here for library

## Development

TODO: Write instructions for development

## TODO

- controllers
- route helpers
- response generation helpers
- simple json generation helpers
- instrumentation/events

## Contributing

1. Fork it ( https://github.com/ragmaanir/tjeneste/fork )
2. Create your feature branch (git checkout -b my-new-feature)
3. Commit your changes (git commit -am 'Add some feature')
4. Push to the branch (git push origin my-new-feature)
5. Create a new Pull Request

## Contributors

- [ragmaanir](https://github.com/ragmaanir) ragmaanir - creator, maintainer
